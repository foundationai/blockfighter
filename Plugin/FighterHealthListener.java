package me.Infectings;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class FighterHealthListener implements Listener
{
	Fighter plugin = null;
	
	public FighterHealthListener(Fighter plugin)
	{
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	//FIX PLAYER SPECIALS... THE MESSAGE COMES TOO EARLY
	private void playerHealth(EntityDamageEvent event)
	{	
		LivingEntity e = (LivingEntity)event.getEntity();
		
		Player p = (Player)e;
		
		p.setFoodLevel(14);
		
		p.getWorld().playEffect(p.getLocation(), Effect.FIREWORK_SHOOT, 1);
		
		if(p.getHealth() == 0 && !(p.getInventory().contains(Material.MAGMA_CREAM) || p.getInventory().contains(Material.EYE_OF_ENDER)))
		{
			return;
		}
		else if((p.getInventory().contains(Material.MAGMA_CREAM) || p.getInventory().contains(Material.EYE_OF_ENDER)) && p.getHealth() <= 12)//SPECIAL
		{
			
			p.sendTitle("", ChatColor.GOLD + "SPECIAL IS READY!");
			
			return;
		}

		else
		{
			return;
		}
		
	}

}
