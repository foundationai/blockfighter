package me.Infectings;
 
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
 
public class FighterCooldown {
 
    // change Main to your plugin's main class
    public Fighter p;
 
    public FighterCooldown(Fighter i) 
    {
    	
        p = i;
    }
   
    int task;
   
    public void setCooldownLength(Player player, int time, HashMap<String, Integer> hashmap) 
    {
    	
        hashmap.put(player.getName(), time);
    }
   
    public int getTimeLeft(Player player, HashMap<String, Integer> hashmap) 
    {
    	
        int time = hashmap.get(player.getName());

        return time;
    }
   
    @SuppressWarnings("deprecation")
	public void startCooldown(final Player player, final HashMap<String, Integer> hashmap, String abilityName) 
    {

    	task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(p , new BukkitRunnable(){
            public void run() 
            {
                int time = hashmap.get(player.getName());

                hashmap.put(player.getName(), time - 1);

            	if(getTimeLeft(player, hashmap) == 0)
                {
            		player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + abilityName + ChatColor.RESET + "IS READY!");
            		
                	hashmap.remove(player.getName());
                	
                	Bukkit.getServer().getScheduler().cancelTask(task);
                	
  
                }
      
            }
            
    	}, 0,20L);
    	
    	
    	
    }
}
 