package me.Infectings;

import org.bukkit.util.Vector;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.DragonFireball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class FighterListener implements Listener
{
    HashMap<String, Integer> mapChar1 = new HashMap<String, Integer>();
    HashMap<String, Integer> mapChar2 = new HashMap<String, Integer>();
    HashMap<String, Integer> mapChar3 = new HashMap<String, Integer>();
    HashMap<String, Integer> mapChar4 = new HashMap<String, Integer>();
    
    Fighter plugin = null;
	
    /*
     * Listens to Player Events
     */
	public FighterListener(Fighter plugin)
	{
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
		this.plugin = plugin;
		
	}
	
	@EventHandler
	private void playerInteract(PlayerInteractEvent event)
	{
		final Player p = event.getPlayer();
		
		Action a = event.getAction();
		
		if(a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK)
		{
			cRyu(p);
			
			cGoku(p);
			
			cNaruto(p);	
			
			cSora(p);
			
			cHiei(p);
		}	
	}
	
//CHARACTERS START ------------------------------ // ------------------------------ //
	//

	//NARUTO----
	//
  
	private void cNaruto(Player p) 
	{
		
	}
	
//-----------------------------------------------------------------------------BEGINS				
	//RYU----
	//

	@SuppressWarnings("deprecation")
	private void cRyu(Player p)
	{
		FighterCooldown ryuC1 = new FighterCooldown(plugin);
		FighterCooldown ryuC2 = new FighterCooldown(plugin);
		FighterCooldown ryuC3 = new FighterCooldown(plugin);
		FighterCooldown ryuC4 = new FighterCooldown(plugin);
       
		if(p.getItemInHand().getType() == Material.BLAZE_POWDER)//HADOUKEN
		{
			String abilityRyu1 = "[HADOUKEN] ";
			
            if (mapChar1.containsKey(p.getName())) 
            {   
                	coolTimeSend(p, abilityRyu1, ryuC1.getTimeLeft(p, mapChar1));
                    
                    return;
            }
            else 
            {
    			p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityRyu1 + ChatColor.RESET + "USED!");
            	
    			Fireball fire = p.getWorld().spawn(p.getEyeLocation(), Fireball.class);
    			
    			fire.setIsIncendiary(false);
    			
    			fire.setFireTicks(0);
    			
    			fire.setVelocity(getFront(p, 3).getDirection());
    			
    			fire.setYield(0);
    			
    			fire.setBounce(false);
    			
    			fire.setShooter(p);
            	
            	ryuC1.setCooldownLength(p, 6, mapChar1);
                
            	ryuC1.startCooldown(p, mapChar1, abilityRyu1);
            	
            	return;
			}
		}
		else if(p.getItemInHand().getType() == Material.FIREWORK_CHARGE)//SHORUYUKEN
		{
			String abilityRyu2 = "[SHORYUKEN] ";
			
            if (mapChar2.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityRyu2, ryuC2.getTimeLeft(p, mapChar2));
            	
                return;
            } 
            else 
            {
               p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityRyu2 + ChatColor.RESET + "USED!");
            	
 	    	  for(int i = 0; i <= 8; i++)
 	    	   {   
 	    		   for(Entity ent : getFront(p, i).getChunk().getEntities())
 	    		   {

     				   p.getWorld().createExplosion(getFront(p,i), 0f);
     				   
     				   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
     				   {
 	    			   ((LivingEntity) ent).damage(3);
 					   ent.setVelocity(ent.getVelocity().setY(.8f));
     				   }
 	    		   }
 	    	   }

            	ryuC2.setCooldownLength(p, 4, mapChar2);
                
            	ryuC2.startCooldown(p, mapChar2, abilityRyu2);
            	
            	return;
			}
			
		}
		else if(p.getItemInHand().getType() == Material.NETHER_STAR)//HURRICANE KICK
		{
			String abilityRyu3 = "[HURRICANE KICK] ";
			
            if (mapChar3.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityRyu3, ryuC3.getTimeLeft(p, mapChar3));
            	
            	return;
            } 
            else 
            {	
                p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityRyu3 + ChatColor.RESET + "USED!");
                
                p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 40 , 1));
            	
	 	    	   for(int i = 0; i <= 6; i++)
		    	   {   
		    		   for(Entity ent : getFront(p,i).getChunk().getEntities())
		    		   {
	    				   p.setVelocity(getFront(p,i).getDirection().multiply(2).setY(.1));
	    				   p.getWorld().createExplosion(getFront(p,i), 0f);
	    				   
	    				   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
	    				   {
		    				   ((LivingEntity) ent).damage(4);
							   ent.setVelocity(ent.getVelocity().setY(1f));
	    				   }

		    		   }
		    	   }
	 	    	   
            	ryuC3.setCooldownLength(p, 5, mapChar3);
                
            	ryuC3.startCooldown(p, mapChar3, abilityRyu3);
            	
            	return;
			}
			
		}
		else if(p.getItemInHand().getType() == Material.FEATHER)//DASH
		{
			String abilityRyu4 = "[DASH] ";
			
            if (mapChar4.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityRyu4, ryuC4.getTimeLeft(p, mapChar4));
            	
                return;
            } 
            else 
            {	
                p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityRyu4 + ChatColor.RESET + "USED!");
            	
	 	    	dashAbility(p);
	 	    	   
            	ryuC4.setCooldownLength(p, 6, mapChar4);
                
            	ryuC4.startCooldown(p, mapChar4, abilityRyu4);
            	
            	return;
			}
			
			
		}
		else if(p.getItemInHand().getType() == Material.MAGMA_CREAM)//SPECIAL SHAKUNETSU HADOUKEN
		{
			if(p.getHealth() < 8)
			{
				p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "[SPECIAL] " + ChatColor.RESET + "USED!");
				
				p.getInventory().remove(Material.MAGMA_CREAM);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 80 , 1));
				
    			Fireball fire = p.getWorld().spawn(p.getEyeLocation(), Fireball.class);
    			
    			fire.setIsIncendiary(false);
    			
    			fire.setFireTicks(0);
    			
    			fire.setVelocity(getFront(p, 1).getDirection());
    			
    			fire.setYield(0);
    			
    			fire.setBounce(false);
    			
    			fire.setShooter(p);
    			
    			for(int i = 0; i <= 10; i++)
	 	    	   {   
	 	    		   for(Entity ent : getFront(p, i).getChunk().getEntities())
	 	    		   {

	     				   p.getWorld().createExplosion(getFront(p,i), 0f);
	     				   
	     				   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
	     				   {
	 	    			   ((LivingEntity) ent).damage(3);
	 					   ent.setVelocity(ent.getVelocity().setY(.5f));
	     				   }
	 	    		   }
	 	    	   }
    			return;
			}
			else
			{
				coolTimeSend(p);
				
				return;
			}
		}
	}
	
//-----------------------------------------------------------------------------BEGINS		
	//GOKU----
	//

	private void cGoku(Player p)
	{
		
	}
	
//-----------------------------------------------------------------------------BEGINS		
	//SORA----
	//

	private void cSora(Player p) 
	{
		
	}
	
	
//-----------------------------------------------------------------------------BEGINS		
	//HIEI----
	//

	@SuppressWarnings("deprecation")
	private void cHiei(Player p)
	{
		FighterCooldown HieiC1 = new FighterCooldown(plugin);
		FighterCooldown HieiC2 = new FighterCooldown(plugin);
		FighterCooldown HieiC3 = new FighterCooldown(plugin);
		FighterCooldown HieiC4 = new FighterCooldown(plugin);
       
		if(p.getItemInHand().getType() == Material.REDSTONE_TORCH_ON)//Dark Flame
		{
			String abilityHiei1 = "[DARKNESS FLAME] ";
			
            if (mapChar1.containsKey(p.getName())) 
            {    
                	coolTimeSend(p, abilityHiei1, HieiC1.getTimeLeft(p, mapChar1));
                    
                    return;
            }
            else 
            {
    			p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityHiei1 + ChatColor.RESET + "USED!");
    			
   	    	  for(int i = 0; i <= 8; i++)
	    	   {   
	    		   for(Entity ent : getFront(p, i).getChunk().getEntities())
	    		   {
	    			   p.getWorld().playEffect(getFront(p,i), Effect.MOBSPAWNER_FLAMES, 1);
    				
	    			   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
    				   {
	    				   
	    			   ((LivingEntity) ent).damage(3);
	    			   
	    			   ent.setVelocity(ent.getVelocity().setY(.5f));
	    			   
	    			   p.getWorld().createExplosion(ent.getLocation(), 0f);
	    			
	    			   
    				   }
	    		   }
	    	   }
            	
            	HieiC1.setCooldownLength(p, 8, mapChar1);
                
            	HieiC1.startCooldown(p, mapChar1, abilityHiei1);
            	
            	return;
			}
		}
		else if(p.getItemInHand().getType() == Material.FERMENTED_SPIDER_EYE)//DRAGON PUNCH
		{
			String abilityHiei2 = "[DRAGON PUNCH] ";
			
            if (mapChar2.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityHiei2, HieiC2.getTimeLeft(p, mapChar2));
            	
                return;
            } 
            else 
            {
              p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityHiei2 + ChatColor.RESET + "USED!");
              
              p.getWorld().playEffect(p.getLocation(), Effect.DRAGON_BREATH , 1);
            	
 	    	  for(int i = 0; i <= 4; i++)
 	    	   {   
 	    		   for(Entity ent : getFront(p, i).getChunk().getEntities())
 	    		   {   
     				
 	    			   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
     				   {
 	    				   
 	    			   ((LivingEntity) ent).damage(3);
 	    			   
 	    			   ent.setVelocity(ent.getVelocity().setY(1f));
 	    			   
 	   			       p.getWorld().playEffect(ent.getLocation(), Effect.DRAGON_BREATH , 1);
 	   			       
 	   			       p.getWorld().strikeLightningEffect(ent.getLocation());
 	   			       
 	   			       p.getWorld().createExplosion(ent.getLocation(), 0f);
 	    			   
     				   }
 	    		   }
 	    	   }

            	HieiC2.setCooldownLength(p, 4, mapChar2);
                
            	HieiC2.startCooldown(p, mapChar2, abilityHiei2);
            	
            	return;
			}
			
		}
		else if(p.getItemInHand().getType() == Material.END_CRYSTAL)//DARK LIGHTNING
		{
			String abilityHiei3 = "[DARK LIGHTNING] ";
			
            if (mapChar3.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityHiei3, HieiC3.getTimeLeft(p, mapChar3));
            	
            	return;
            } 
            else 
            {	
                p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityHiei3 + ChatColor.RESET + "USED!");
                
                p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 45 , 1));
            	
	 	    	   for(int i = 0; i <= 6; i++)
		    	   {   
		    		   for(Entity ent : getFront(p,i).getChunk().getEntities())
		    		   {
	    				   p.setVelocity(getFront(p,i).getDirection().multiply(2).setY(.1));
	    				   p.getWorld().strikeLightningEffect(getFront(p,i));
	    				   
	    				   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
	    				   {
		    				   ((LivingEntity) ent).damage(8);
							   ent.setVelocity(ent.getVelocity().setY(1.5f));
	    				   }

		    		   }
		    	   }
	 	    	   
            	HieiC3.setCooldownLength(p, 10, mapChar3);
                
            	HieiC3.startCooldown(p, mapChar3, abilityHiei3);
            	
            	return;
			}
			
		}
		else if(p.getItemInHand().getType() == Material.FEATHER)//DASH
		{
			String abilityHiei4 = "[DASH] ";
			
            if (mapChar4.containsKey(p.getName())) 
            {
            	coolTimeSend(p, abilityHiei4, HieiC4.getTimeLeft(p, mapChar4));
            	
                return;
            } 
            else 
            {	
                p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + abilityHiei4 + ChatColor.RESET + "USED!");
            	
	 	    	dashAbility(p);
	 	    	   
            	HieiC4.setCooldownLength(p, 6, mapChar4);
                
            	HieiC4.startCooldown(p, mapChar4, abilityHiei4);
            	
            	return;
			}
			
			
		}
		else if(p.getItemInHand().getType() == Material.EYE_OF_ENDER)//SPECIAL Dragon of the Darkness Flame
		{
			if(p.getHealth() < 8)
			{
				p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "[SPECIAL] " + ChatColor.RESET + "USED!");
				
				p.getInventory().remove(Material.EYE_OF_ENDER);
				
	            p.getWorld().playEffect(p.getLocation(), Effect.DRAGON_BREATH , 1);
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 80 , 1));
				
				p.getWorld().playEffect(p.getLocation(), Effect.ENDERDRAGON_GROWL, 1);
    			
    			for(int i = 0; i <= 10; i++)
	 	    	   {   
    				
    				p.getWorld().playEffect(getFront(p,i), Effect.MOBSPAWNER_FLAMES, 1);
    				
	 	    		   for(Entity ent : getFront(p, i).getChunk().getEntities())
	 	    		   {
	 	    			   
	     				   if((ent instanceof LivingEntity && ent != p && getFront(p, i).distanceSquared(ent.getLocation()) <= (float)i) || (ent instanceof LivingEntity && p.getEyeLocation().distanceSquared(ent.getLocation()) <= (float)i && ent != p))
	     				   {
	     					   
	     				   p.getWorld().strikeLightningEffect(ent.getLocation());
	 	    			   
	     				   ((LivingEntity) ent).damage(6);
	 	    			   
	 					   ent.setVelocity(ent.getVelocity().setY(.5f));
	 					   
	     				   }
	 	    		   }
	 	    	   }
    			return;
			}
			else
			{
				coolTimeSend(p);
				
				return;
			}
		}
	}
	

//
//-----------------------------------------------------------------------------ENDS
	
	
			
//
//CHARACTERS FINISH ------------------------------ // ------------------------------ //
	
	/*
	 * SENDS PLAYER DASHING BY USING GETFRONT METHOD THEN INSCREASING VELOCITY FOR A SHORT AMOUNT OF TIME
	 */
	private void dashAbility(Player p)
	{
		p.setVelocity(getFront(p,4).getDirection().multiply(1.5).setY(.1));
		
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 40 , 1));
		
	}
	
	/*
	 * GETS FRONT OF PLAYER 
	 */
	private Location getFront(Player user, int range)
	{
		Location loc = user.getEyeLocation();
        Vector direction = loc.getDirection().normalize();
		
		for(int i = 0; i <= range; i++)
		{
			loc.add(direction).getBlock();
		}
		
		return loc;
	}

	private void coolTimeSend(Player p, String ability, int time)
	{
		
		p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + ability + ChatColor.RESET + 
	        		ChatColor.WHITE + "IS NOT READY! WAIT "+ ChatColor.YELLOW 
	        		+ time  + ChatColor.WHITE +" seconds!");
	}
	
	private void coolTimeSend(Player p)
	{
		p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[SPECIAL] " + ChatColor.RESET + 
        		ChatColor.WHITE + "IS NOT READY!");
	}

}